<?php

namespace studiocreativateam\ResponsiveImages;

use SilverStripe\Admin\AdminRootController;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Extension;

class ImageExtension extends Extension
{
    private static $add_lazyload_class = true;
    private static $add_cache_key = true;

    public function Scale($percentage)
    {
        $variant = $this->owner->variantName(__FUNCTION__, $percentage);
        return $this->owner->manipulateImage($variant, function (\SilverStripe\Assets\Image_Backend $backend) use ($percentage) {
            if ($percentage == 100) return $this->owner;
            return $backend->resizeByWidth(round($backend->getWidth() * $percentage / 100));
        });
    }

    public function getExtension()
    {
        if ($this->owner->force_webp) return 'webp';
        return $this->owner->getExtension();
    }

    public function getKey()
    {
        if (Config::inst()->get(__CLASS__, 'add_cache_key')) {
            return substr($this->owner->FileHash, 0, 3);
        }
    }

    // public function onAfterPublish($original)
    // {
    //   $liveOwners = $original->findOwners();
    //   foreach($liveOwners as $owner)
    //   {
    //     var_dump($owner);
    //   }
    // }

    public function getAddLazyLoadClass()
    {
        if (Controller::curr()->getRequest()->param('Controller') == AdminRootController::class) return false;
        return Config::inst()->get(__CLASS__, 'add_lazyload_class');
    }
}